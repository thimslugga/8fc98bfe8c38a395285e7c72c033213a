#!/bin/bash
function process_script_request {
  if [[ $# -eq 0 ]]; then
    show_help
  else
    case $1 in
      help) show_help;;
      back) serve_backend;;
      back:prod) serve_backend_production;;
      front) update_and_watch_assets;;
      front:prod) build_assets_for_deployment;;
      logs) show_logs;;
      deploy) deploy_app;;
      redeploy) redeploy_app;;
      deploy:test) deploy_test_app;;
      gen:local) generate_local_conf;;
    esac
  fi
}

function show_help {
  echo "What to do?"
  echo "help  back  front   logs  deploy  deploy:test"
}

function serve_backend {
  sails lift
}

function serve_backend_production {
  NODE_ENV=production node app.js
}

function update_and_watch_assets {
  cd angular
  ng build --watch --extract-css
  cd ../
}

function show_logs {
  heroku logs --tail --app abracadabra
}

function build_assets_for_deployment {
  cd angular
  ng build --prod --extract-css --output-path ../assets
  cd ../
  git add assets
  git commit -m "updating assets for deployment"
}

function deploy_app {
  build_assets_for_deployment
  git co production
  git merge master
  git push
  git co master
}

function deploy_test_app {
  git push -f heroku-test master
}

function generate_local_conf {
  localjs="config/local.js"

  # TODO Лучше записат массив в цикл. А ещё лучше — в
  # <<- EndOfText
  #   Text
  #   Text
  #   Text
  # EndOfText

  echo "module.exports = {"  > $localjs
  echo "  paths: {"  >> $localjs
  echo "    public: 'angular/dist'"  >> $localjs
  echo "  },"  >> $localjs
  echo ""  >> $localjs
  echo "  datastores: {"  >> $localjs
  echo "    default: {"  >> $localjs
  echo "      adapter: 'sails-disk'"  >> $localjs
  echo "    },"  >> $localjs
  echo ""  >> $localjs
  echo "    mysql: {"  >> $localjs
  echo "      adapter: require('sails-mysql'),"  >> $localjs
  echo "      url: `mysql://username:password@server:3306/database`"  >> $localjs
  echo "    }"  >> $localjs
  echo "  },"  >> $localjs
  echo ""  >> $localjs
  echo "  mail: {"  >> $localjs
  echo "    MAILER_SERVICE: 'gmail',"  >> $localjs
  echo "    MAILER_EMAIL: 'e@ma.il',"  >> $localjs
  echo "    MAILER_PASS: 'password',"  >> $localjs
  echo "    ADMIN_EMAILS: 'e@ma.il'"  >> $localjs
  echo "  }"  >> $localjs
  echo "};"  >> $localjs
}

function redeploy_app {
  heroku restart --app abracadabra
}

# TODO Сгенерировать новый хэш для secret в config/session.js

process_script_request $1
